from django.test import Client, TestCase
from django.urls import resolve
from .views import index, add_friend, validate_npm, delete_friend, friend_list, friend_list_json
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_friend_list_json_data_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list-json/')
        self.assertEqual(response.status_code, 200)

    def test_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])

    def test_add_friend(self):
        response_post = Client().post(
            '/lab-7/add-friend/', 
            {'name':"putri", 'npm':"1606917802",}
        )
        self.assertEqual(response_post.status_code, 200)

    def test_invalid_sso_raise_exception(self):
        username = "putri"
        password = "password"
        csui_helper = CSUIhelper()
        with self.assertRaises(Exception) as context:
            csui_helper.instance.get_access_token(username, password)
        self.assertIn("putri", str(context.exception))

    def test_validate_npm(self):
        response = self.client.post('/lab-7/validate-npm/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(html_response, {'is_taken':False})

    def test_friend_as_dict(self):
        friend = Friend.objects.create(friend_name="Pina Korata", npm="1606123456")
        entry = Friend.objects.get(pk=1)
        dictionary = entry.as_dict()
        self.assertEqual("Pina Korata", dictionary['friend_name'])
        self.assertEqual("1606123456", dictionary['npm'])

    def test_delete_friend(self):
        friend = Friend.objects.create(friend_name="Pina Korata", npm="1606123456")
        response = Client().post('/lab-7/friend-list/delete-friend/' + str(friend.npm) + '/')
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(friend, Friend.objects.all())
